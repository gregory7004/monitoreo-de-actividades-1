<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class Actividades extends Model
{
    Protected $table='Actividades';

    protected $primaryKey='Id_Actividad_';

    public $timestamps=false;

    protected $fillable =[
        'Descripcion_Actividad',
        'Fecha_Inicio',
        'Fecha_Fin',
        'Estado',
        'Usuario_Responsable_Actividad',
        'MetaFK'
    ];

    public function meta()
    {
        return $this->belongsTo(Meta::class,'MetaFK','Id_Meta','Metas')->withDefault();
    }
    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'Usuario_Responsable_Actividad','idUsuarios','Usuarios')->withDefault();
    }
}
