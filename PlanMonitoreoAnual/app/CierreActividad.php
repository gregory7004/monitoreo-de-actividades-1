<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class CierreActividad extends Model
{
    Protected $table='Cierre_Actividades';

    protected $primaryKey='idCierre_Actividades';

    public $timestamps=false;

    protected $fillable =[
        'Anexos',
        'Imagenes',
        'Estado'
    ];

    public function actividad()
    {
        return $this->belongsTo(Actividades::class,'Id_Actividad_Cerrar','Id_Actividad_','Actividades')->withDefault();
    }
    public function meta()
    {
        return $this->belongsTo(Meta::class,'MetaFK','Id_Meta','Metas')->withDefault();
    }
    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'Usuario_Responsable_Actividad','idUsuarios','Usuarios')->withDefault();
    }
}