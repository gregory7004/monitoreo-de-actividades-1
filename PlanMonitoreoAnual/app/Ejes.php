<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class Ejes extends Model
{
    Protected $table='Ejes';

    protected $primaryKey='idEjes';

    public $timestamps=false;

    protected $fillable =[
        'Descripcion_eje',
        'Unidad_administrativa',
        'Estado'
    ];

    public function unidad()
    {
        return $this->belongsTo(UnidadAdministrativa::class,'Unidad_administrativa','idUnidad_administrativa','Ejes')->withDefault();
    }
}
