<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use PlanMonitoreoAnual\Actividades;
use Illuminate\support\facades\redirect;
use PlanMonitoreoAnual\Http\Requests\ActividadesFormRequest;
use PlanMonitoreoAnual\Meta;
use PlanMonitoreoAnual\Usuario;
use DB;

class ActividadesController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {

        // $linea=linea::with('objetivos')->get();
        // print_r($linea->toArray());
        
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $actividades=Actividades::with('meta')->get();
            $usuario=Actividades::with('usuario')->get();
            $actividades=Actividades::where('Descripcion_Actividad','like', '%'.$query.'%','and','Estado','=','Activado')->get();
            return view ("actividades.index",["actividades"=>$actividades,"searchText"=>$query,"usuario"=>$usuario]);
            }
            else{
                $actividades=Actividades::with('meta')->get();
                $usuario=Actividades::with('usuario')->get();
                // $actividades=Actividades::where('Estado','=','Pendiente')->get();
                // print_r($usuario->toArray());
                return view ("actividades.index",["actividades"=>$actividades,"searchText"=>$query,"usuario"=>$usuario]);
            }
        }
    }
    public function create()
    {
        $usuario=DB::table('Usuarios')->where('estado','=','Activado')->get();
        $meta=DB::table('Metas')->where('estado','=','Activado')->get();
        // print_r($meta->toArray());
        return view("actividades.create",["meta"=>$meta,"usuario"=>$usuario]);
    }
    public function store (ActividadesFormRequest $request)
    {
        $Actividades=new Actividades;
        $Actividades->Descripcion_Actividad=$request->get('Descripcion_Actividad');
        $Actividades->Fecha_Inicio=$request->get('Fecha_Inicio');
        $Actividades->Fecha_Fin=$request->get('Fecha_Fin');
        $Actividades->Usuario_Responsable_Actividad=$request->get('Usuario_Responsable_Actividad');
        $Actividades->MetaFK=$request->get('MetaFK');
        $Actividades->Estado='Pendiente';
        $Actividades->save();
        return Redirect::to('actividades');
    }
    public function show($id)
    {
        return view("actividades.show",["actividades"=>actividades::FindOrFail($id)]);
    }
    public function edit($id)
    {
        $actividades=Actividades::all()->where('Id_Actividad_','=',$id)->first();
        $usuario=DB::table('Usuarios')->where('estado','=','Activado')->get();
        $meta=DB::table('Metas')->where('estado','=','Activado')->get();
        // print_r($actividades->toArray());
        return view("actividades.edit",["actividades"=>$actividades,"usuario"=>$usuario,"meta"=>$meta]);
    }
    public function update(ActividadesFormRequest $request, $id)
    {
       
        $actividades=Actividades::where('Id_Actividad_',$id)->first();
        $actividades->Descripcion_Actividad=$request->input('Descripcion_Actividad');
        $actividades->Fecha_Inicio=$request->input('Fecha_Inicio');
        $actividades->Fecha_Fin=$request->input('Fecha_Fin');
        $actividades->Usuario_Responsable_Actividad=$request->input('Usuario_Responsable_Actividad');
        $actividades->MetaFK=$request->input('MetaFK');
        $actividades->update();
        return Redirect::to('actividades');
    }
    public function destroy($id)
    {
        
        $actividades=Actividades::all()->where('Id_Actividad_',$id)->first();
        $actividades->delete();
        return Redirect::to('actividades');
    }
}
