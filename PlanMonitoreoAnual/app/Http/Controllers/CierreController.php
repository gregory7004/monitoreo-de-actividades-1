<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use PlanMonitoreoAnual\CierreActividad;
use PlanMonitoreoAnual\Usuario;
use PlanMonitoreoAnual\Meta;
use PlanMonitoreoAnual\Actividades;
use Illuminate\support\facades\redirect;
// use Illuminate\support\facades\Input;
use PlanMonitoreoAnual\Http\Requests\CierreActividadFormRequest;
use DB;

class CierreController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $actividades=Actividades::with('meta')->get();
            $usuario=Actividades::with('usuario')->get();
            $actividades=Actividades::where('Descripcion_Actividad','like', '%'.$query.'%','and','Estado','=','Pendiente')->get();
            // $actividades=Actividades::where('Estado','=','Pendiente')->get();
            return view ("cierre.index",["actividades"=>$actividades,"searchText"=>$query,"usuario"=>$usuario]);
            }
            else{
                $actividades=Actividades::with('meta')->get();
                $actividades=Actividades::where('Estado','=','Pendiente')->get();
                $usuario=Actividades::with('usuario')->get();
                // print_r($usuario->toArray());
                return view ("cierre.index",["actividades"=>$actividades,"searchText"=>$query,"usuario"=>$usuario]);
            }
        }
    }
    public function create()
    {
        // $unidad=DB::table('Unidad_administrativa')->where('Estado','=','1')->get();
        // // print_r($ejes->toArray());
        // return view("ejes.create",["unidad"=>$unidad]);
    }
    public function store(EjesFormRequest $request)
    {
        // $ejes=new Ejes;
        // $ejes->Descripcion_eje=$request->get('Descripcion_eje');
        // $ejes->Unidad_administrativa=$request->get('Unidad_administrativa');
        // $ejes->estado='1';
        // $ejes->save();
        // return Redirect::to('ejes');
    }
    public function show($id)
    {
        return view("cierre.show",["cierre"=>CierreActividad::FindOrFail($id)]);
    }
    public function edit($id)
    {
        $meta=Actividades::with('meta')->get();
        $usuario=Actividades::with('usuario')->get();
        $actividades=Actividades::all()->where('Id_Actividad_','=',$id)->first();
        // print_r($meta->toArray());
        return view("cierre.edit",["actividades"=>$actividades,"usuario"=>$usuario,"meta"=>$meta]);
    }
    public function update(CierreActividadFormRequest $request, $id)
    {
        $actividades=Actividades::FindOrFail($id);

        // return $request->file('avatar');


        if(Input::hasFile('imagen')){
            $file=Input::file('imagen');
            $file->move(public_path().'/imagenes/imagentareas/',$file->getClientOriginalName());
            $actividades->imagen=$file->getClientOriginalName();
        }
        if(Input::hasFile('anexos')){
            $file=Input::file('anexos');
            $file->move(public_path().'/imagenes/anexostareas/',$file->getClientOriginalName());
            $actividades->anexos=$file->getClientOriginalName();
        }
        $actividades->nota=$request->get('nota');
        $actividades->update();
        return Redirect::to('cierreactividades');
    }
    public function destroy($id)
    {
        $ejes=Ejes::FindOrFail($id);
        $ejes->estado=0;
        $ejes->update();
        return Redirect::to('ejes');
    }
}
