<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use PlanMonitoreoAnual\ejes;
use Illuminate\support\facades\redirect;
use PlanMonitoreoAnual\Http\Requests\EjesFormRequest;
use PlanMonitoreoAnual\UnidadAdministrativa;

use DB;

class EjesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));

            if(!empty($query))
            {
                $ejes=Ejes::where('Descripcion_eje','LIKE','%'.$query.'%')->get();
                // ->where('estado','=','1')
                // ->paginate(10);
                $Unidad=Ejes::with('unidad')->get();
                // print_r( $ejes->toArray());
                return view ("ejes.index",["ejes"=>$ejes,"searchText"=>$query]);
            }
            else
            {
                $ejes=Ejes::with('unidad')->get();
                // print_r( $ejes->toArray());
                return view ("ejes.index",["ejes"=>$ejes,"searchText"=>$query]);
            }

        }
    }
    public function create()
    {
        $unidad=DB::table('Unidad_administrativa')->where('Estado','=','1')->get();
        // print_r($ejes->toArray());
        return view("ejes.create",["unidad"=>$unidad]);
    }
    public function store(EjesFormRequest $request)
    {
        $ejes=new Ejes;
        $ejes->Descripcion_eje=$request->get('Descripcion_eje');
        $ejes->Unidad_administrativa=$request->get('Unidad_administrativa');
        $ejes->estado='1';
        $ejes->save();
        return Redirect::to('ejes');
    }
    public function show($id)
    {
        return view("ejes.show",["ejes"=>Ejes::FindOrFail($id)]);
    }
    public function edit($id)
    {
        $unidad=DB::table('Unidad_administrativa')->where('Estado','=','1')->get();
        return view("ejes.edit",["ejes"=>Ejes::FindOrFail($id),"unidad"=>$unidad,]);
    }
    public function update(EjesFormRequest $request, $id)
    {
        $ejes=Ejes::FindOrFail($id);
        $ejes->Descripcion_eje=$request->get('Descripcion_eje');
        $ejes->Unidad_administrativa=$request->get('Unidad_administrativa');
        $ejes->update();
        return Redirect::to('ejes');
    }
    public function destroy($id)
    {
        $ejes=Ejes::FindOrFail($id);
        $ejes->estado=0;
        $ejes->update();
        return Redirect::to('ejes');
    }
}
