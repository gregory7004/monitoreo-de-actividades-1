<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\support\facades\redirect;
use Illuminate\Http\Request;
use PlanMonitoreoAnual\Http\Requests\LineaActuacionFormRequest;
use PlanMonitoreoAnual\LineaActuacion as linea;
use PlanMonitoreoAnual\Objetivos;
use DB;

class LineaActuacionController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {

        // $linea=linea::with('objetivos')->get();
        // print_r($linea->toArray());
        
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $linea=linea::with('objetivos')->get();
            $linea=linea::where('desc_linea_actuacion','like', '%'.$query.'%')->get();
            return view ("linea.index",["linea"=>$linea,"searchText"=>$query]);
            }
            else{
                $linea=linea::with('objetivos')->get();
                return view ("linea.index",["linea"=>$linea,"searchText"=>$query]);
            }
        }
    }
    public function create()
    {
        $objetivos=DB::table('Objetivos')->where('estado','=','1')->get();
        return view("linea.create",["objetivos"=>$objetivos]);
    }
    public function store (LineaActuacionFormRequest $request)
    {
        $linea=new linea;
        $linea->desc_linea_actuacion=$request->get('desc_linea_actuacion');
        $linea->id_obj_estr=$request->get('id_obj_estr');
        $linea->estado='Activado';
        $linea->save();
        return Redirect::to('lineaactuacion');
    }
    public function show($id)
    {
        return view("linea.show",["linea"=>linea::FindOrFail($id)]);
    }
    public function edit($id)
    {
        $linea=linea::all()->where('idlinea_actuacion','=',$id)->first();
        $objetivos=Objetivos::all()->where('Estado','=','1');
        // print_r($objetivos->toArray());
        return view("linea.edit",["linea"=>$linea,"objetivos"=>$objetivos]);
    }
    public function update(LineaActuacionFormRequest $request, $id)
    {
       
        $linea=linea::where('idlinea_actuacion',$id)->first();
        $linea->desc_linea_actuacion=$request->input('desc_linea_actuacion');
        $linea->id_obj_estr=$request->input('id_obj_estr');
        $linea->update();
        return Redirect::to('lineaactuacion');
    }
    public function destroy($id)
    {
        
        $linea=linea::all()->where('idlinea_actuacion',$id)->first();
        $linea->Estado='0';
        $linea->update();
        return Redirect::to('lineaactuacion');
    }
}
