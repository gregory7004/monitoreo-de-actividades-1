<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\facades\redirect;
use PlanMonitoreoAnual\Http\Requests\MetasFormRequest;
use PlanMonitoreoAnual\Meta as meta;
use PlanMonitoreoAnual\ObjetivoTactico;

class MetasController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
        
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $Metas=Meta::with('objetivosp')->get();
            $Metas=Meta::where('Descripcion','like', '%'.$query.'%')->get();
            return view ("meta.index",["Meta"=>$Metas,"searchText"=>$query]);
            }
            else{
                $Metas=Meta::with('objetivosp')->get();
            return view ("meta.index",["Meta"=>$Metas,"searchText"=>$query]);
            }
        }
    }
    public function create()
    {
        $Objetivos=ObjetivoTactico::all()->where('estado','=','Activado');
        // print_r($Objetivos->toArray());
        return view("meta.create",["Objetivos"=>$Objetivos]);
    }
    public function store (MetasFormRequest $request)
    {
        $Meta = new meta;
        $Meta->Descripcion=$request->get('Descripcion');
        $Meta->obje_tac=$request->get('obje_tac');
        $Meta->estado='Activado';
        $Meta->save();
        return Redirect::to('meta');
    }
    public function show($id)
    {
        return view("meta.show",["Meta"=>Meta::FindOrFail($id)]);
    }
    public function edit($id)
    {
        $Metas=meta::all()->where('Id_Meta','=',$id)->first();
        $Objetivos=ObjetivoTactico::all()->where('estado','=','Activado');
        // print_r($Objetivo->toArray());
        return view("meta.edit",["Meta"=>$Metas,"Objetivo"=>$Objetivos]);
    }
    public function update(MetasFormRequest $request, $id)
    {
        $Metas=meta::where('Id_Meta',$id)->first();
        // print_r($Metas);
        $Metas->Descripcion=$request->input('Descripcion');
        $Metas->obje_tac=$request->input('obje_tac');
        $Metas->update();
        return Redirect::to('meta');
    }
    public function destroy($id)
    {
        
        $Objetivo=Objetivos::all()->where('idObjetivos',$id)->first();
        $Objetivo->Estado='0';
        $Objetivo->update();
        return Redirect::to('objetivo');
    }
}
