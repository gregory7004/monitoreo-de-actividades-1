<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\facades\redirect;
use PlanMonitoreoAnual\Http\Requests\ObjetivoTacticoFormRequest;
use PlanMonitoreoAnual\LineaActuacion as linea;
use PlanMonitoreoAnual\ObjetivoTactico as objetivo;
use DB;

class ObjetivoTacticoController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
        
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $Objetivos=objetivo::with('linea')->get();
            $Objetivos=objetivo::where('desc_objetivo','like', '%'.$query.'%')->get();
            return view ("objetivotac.index",["Objetivo"=>$Objetivos,"searchText"=>$query]);
            }
            else{
                $Objetivos=objetivo::with('linea')->get();
                // print_r($Objetivos->toArray());
            return view ("objetivotac.index",["Objetivo"=>$Objetivos,"searchText"=>$query]);
            }
        }
    }
    public function create()
    {
        $linea=DB::table('linea_actuacion')->where('estado','=','Activado')->get();
        // print_r($linea->toArray());
        return view("objetivotac.create",["linea"=>$linea]);
    }
    public function store (ObjetivoTacticoFormRequest $request)
    {
        $objetivo=new objetivo;
        $objetivo->desc_objetivo=$request->get('desc_objetivo');
        $objetivo->id_linea_actuacion=$request->get('id_linea_actuacion');
        $objetivo->estado='Activado';
        $objetivo->save();
        return Redirect::to('objetivotactico');
    }
    public function show($id)
    {
        return view("objetivotac.show",["objetivo"=>objetivo::FindOrFail($id)]);
    }
    public function edit($id)
    {
     
        $objetivo=objetivo::all()->where('idobjetivo_tactico','=',$id)->first();
        $linea=linea::all()->where('estado','=','Activado');
        // print_r($linea->toArray());
        return view("objetivotac.edit",["linea"=>$linea,"objetivo"=>$objetivo]);
    }
    public function update(ObjetivoTacticoFormRequest $request, $id)
    {
       
        $objetivo=objetivo::where('idobjetivo_tactico',$id)->first();
        $objetivo->desc_objetivo=$request->input('desc_objetivo');
        $objetivo->id_linea_actuacion=$request->input('id_linea_actuacion');
        $objetivo->update();
        return Redirect::to('objetivotactico');
    }
    public function destroy($id)
    {
        $objetivo=objetivo::all()->where('idobjetivo_tactico',$id)->first();
        $objetivo->estado='Desactivado';
        $objetivo->update();
        return Redirect::to('objetivotac');
    }
}
