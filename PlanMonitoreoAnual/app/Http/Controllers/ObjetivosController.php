<?php

namespace PlanMonitoreoAnual\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\facades\redirect;
use PlanMonitoreoAnual\Http\Requests\ObjetivosFormRequest;
use PlanMonitoreoAnual\Objetivos;
use PlanMonitoreoAnual\Ejes as Ejes;
use DB;

class ObjetivosController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
        
        if ($request)
        {
            
            $query=trim($request->get('searchText'));

            if(!empty($query)){
                
            $Objetivos=Objetivos::with('ejes')->get();
            $Objetivos=Objetivos::where('Descripcion_objetivo','like', '%'.$query.'%')->get();
            return view ("objetivo.index",["Objetivo"=>$Objetivos,"searchText"=>$query]);
            }
            else{
                $Objetivos=Objetivos::with('ejes')->get();
            return view ("objetivo.index",["Objetivo"=>$Objetivos,"searchText"=>$query]);
            }
        }
    }
    public function create()
    {
        $ejes=DB::table('Ejes')->where('estado','=','1')->get();
        return view("objetivo.create",["ejes"=>$ejes]);
    }
    public function store (ObjetivosFormRequest $request)
    {
        $Objetivo=new Objetivos;
        $Objetivo->Id_eje=$request->get('Id_eje');
        $Objetivo->Descripcion_objetivo=$request->get('Descripcion_objetivo');
        $Objetivo->Presupuesto=$request->get('Presupuesto');
        $Objetivo->estado='1';
        $Objetivo->save();
        return Redirect::to('objetivo');
    }
    public function show($id)
    {
        return view("Objetivo.show",["objetivo"=>objetivo::FindOrFail($id)]);
    }
    public function edit($id)
    {
     
        $Objetivo=Objetivos::all()->where('idObjetivos','=',$id)->first();
        $ejes=Ejes::all()->where('Estado','=','1');
        // print_r($Objetivo->toArray());
        return view("objetivo.edit",["ejes"=>$ejes,"Objetivo"=>$Objetivo]);
    }
    public function update(ObjetivosFormRequest $request, $id)
    {
       
        $Objetivo=Objetivos::where('idObjetivos',$id)->first();
        $Objetivo->Id_eje=$request->input('Id_eje');
        $Objetivo->Descripcion_objetivo=$request->input('Descripcion_objetivo');
        $Objetivo->Presupuesto=$request->input('Presupuesto');
        $Objetivo->update();
        return Redirect::to('objetivo');
    }
    public function destroy($id)
    {
        
        $Objetivo=Objetivos::all()->where('idObjetivos',$id)->first();
        $Objetivo->Estado='0';
        $Objetivo->update();
        return Redirect::to('objetivo');
    }
}
