<?php

namespace PlanMonitoreoAnual\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActividadesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Descripcion_Actividad'=>'required|max:250',
            'Fecha_Inicio'=>'required',
            'Fecha_Fin'=>'required',
            'Usuario_Responsable_Actividad'=>'required',
            'MetaFK'=>'required'
        ];
    }
}
