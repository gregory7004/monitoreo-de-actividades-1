<?php

namespace PlanMonitoreoAnual\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EjesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *Nombres de imputs del form
     * @return array
     */
    public function rules()
    {
        return [
            'Descripcion_eje'=>'required|max:200',
            'Unidad_administrativa'=>'required|max:5',
        ];
    }
}
