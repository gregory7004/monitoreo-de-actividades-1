<?php

namespace PlanMonitoreoAnual\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObjetivosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'Id_eje'=> 'required',
        'Descripcion_objetivo'=>'required|max:250',
        'Presupuesto' =>'required'
        ];
    }
}
