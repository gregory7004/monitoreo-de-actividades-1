<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class LineaActuacion extends Model
{
    Protected $table='linea_actuacion';

    protected $primaryKey='idlinea_actuacion';

    public $timestamps=false;

    protected $fillable =[
        'desc_linea_actuacion',
        'id_obj_estr',
        'estado'
    ];

    public function objetivos()
    {
        return $this->belongsTo(Objetivos::class,'id_obj_estr','idObjetivos','linea_actuacion')->withDefault();
    }
}
