<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    Protected $table='Metas';

    protected $primaryKey='Id_Meta';

    public $timestamps=false;

    protected $fillable =[
        'Descripcion',
        'Estado',
        'obje_tac'
    ];

    public function objetivosp()
    {
        return $this->belongsTo(ObjetivoTactico::class,'obje_tac','idobjetivo_tactico','objetivo_tactico')->withDefault();
    }
}
