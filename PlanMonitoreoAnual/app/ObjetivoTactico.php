<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class ObjetivoTactico extends Model
{
    Protected $table='objetivo_tactico';

    protected $primaryKey='idobjetivo_tactico';

    public $timestamps=false;

    protected $fillable =[
        'desc_objetivo',
        'id_linea_actuacion',
        'estado'
    ];

    public function linea()
    {
        return $this->belongsTo(LineaActuacion::class,'id_linea_actuacion','idlinea_actuacion','linea_actuacion')->withDefault();
    }
}
