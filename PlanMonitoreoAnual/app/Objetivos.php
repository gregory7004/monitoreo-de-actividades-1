<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;


class Objetivos extends Model
{
    Protected $table='Objetivos';

    protected $primaryKey='idObjetivos';

    public $timestamps=false;

    protected $fillable =[
        'Id_eje',
        'Descripcion_objetivo',
        'Presupuesto',
        'Estado'
    ];

    public function ejes()
    {
        return $this->belongsTo(Ejes::class,'Id_eje','idEjes','objetivos')->withDefault();
    }
}
