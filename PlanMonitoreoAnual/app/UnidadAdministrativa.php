<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class UnidadAdministrativa extends Model
{
    Protected $table='unidad_administrativa';

    protected $primaryKey='idUnidad_administrativa';

    public $timestamps=false;

    protected $fillable =[
        'Descripcion',
        'Estado'
    ];
}
