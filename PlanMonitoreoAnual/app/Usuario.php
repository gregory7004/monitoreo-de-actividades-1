<?php

namespace PlanMonitoreoAnual;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    Protected $table='Usuarios';

    protected $primaryKey='idUsuarios';

    public $timestamps=false;

    protected $fillable =[
        'Nombres',
        'Apellidos',
        'Unidad_administrativa',
        'roll',
        'estado',
        'Correo_Electronico'
    ];
}
