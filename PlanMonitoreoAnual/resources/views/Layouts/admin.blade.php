
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MoniOP | Sistema Monitoreo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/ejes') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>PF</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Plan Monitoreo Anual</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <small class="bg-red">Online</small>
                  <span class="hidden-xs">Usuario</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    
                    <p>
                      Desarrollado por PerfectSoft
                      <small>www.PerfectSof.com</small>
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="pull-right">
                      <a href="{{route('logout')}}" class="btn btn-default btn-flat">Cerrar</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
                    
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-align-justify"></i>
                <span>Registrar datos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/ejes/create')}}"><i class="fa fa-circle-o"></i> Ejes</a></li>
                <li><a href="{{ url('/unidad/create')}}"><i class="fa fa-circle-o"></i> Unidad Administrativa</a></li>
                <li><a href="{{ url('/objetivo/create')}}"><i class="fa fa-circle-o"></i> Objetivos Estratégicos</a></li>
                <li><a href="{{ url('/lineaactuacion/create')}}"><i class="fa fa-circle-o"></i> Líneas de Actuación</a></li>
                <li><a href="{{ url('/objetivotactico/create')}}"><i class="fa fa-circle-o"></i> Objetivos Tácticos</a></li>
                <li><a href="{{ url('/meta/create')}}"><i class="fa fa-circle-o"></i> Metas</a></li>
                <li><a href="{{ url('/actividades/create')}}"><i class="fa fa-circle-o"></i> Actividades</a></li>
                <!-- <li><a href="{{ url('/ejes') }}"><i class="fa fa-circle-o"></i> Consultar</a></li> -->
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-search"></i>
                <span>Consultar datos</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/ejes') }}"><i class="fa fa-circle-o"></i> Ejes</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Unidad Administrativa</a></li>
                <li><a href="{{ url('/objetivo')}}"><i class="fa fa-circle-o"></i> Objetivos Estratégicos</a></li>
                <li><a href="{{ url('/lineaactuacion')}}"><i class="fa fa-circle-o"></i> Líneas de Actuación</a></li>
                <li><a href="{{ url('/objetivotactico')}}"><i class="fa fa-circle-o"></i> Objetivos Tácticos</a></li>
                <li><a href="{{ url('/meta')}}"><i class="fa fa-circle-o"></i> Metas</a></li>
                <li><a href="{{ url('/actividades')}}"><i class="fa fa-circle-o"></i> Actividades Pendientes</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Actividades Realizadas</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-times-circle"></i>
                <span>Cierre de actividades</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/cierreactividades')}}"><i class="fa fa-circle-o"></i> Cerrar Actividad</a></li>
                
              </ul>
            </li>
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-clock-o"></i>
                <span>Actividades</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Registrar</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Consultar</a></li>
               
              </ul>
            </li> -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-flag"></i>
                <span>Reportes</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Actividades realizadas</a></li>
                <li><a href="{{ url('/cierreactividades')}}"><i class="fa fa-circle-o"></i> Actividaes faltantes</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Rendición por período</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Acceso</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="configuracion/usuario"><i class="fa fa-circle-o"></i> Usuarios</a></li>
                
              </ul>
            </li>
             <li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span>Ayuda</span>
                <small class="label pull-right bg-red">PDF</small>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-info-circle"></i> <span>Acerca De...</span>
                <small class="label pull-right bg-yellow">PS</small>
              </a>
            </li>
                        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>





       <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Sistema de Monitoreo</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
	                  	<div class="col-md-12">
		                          <!--Contenido-->
                              @yield('contenido')
		                          <!--Fin Contenido-->
                           </div>
                        </div>
		                    
                  		</div>
                  	</div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <!--Fin-Contenido-->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Versión</b> 1.0
        </div>
        <strong>Copyright &copy; 2020 <a href="">PerfectSoft</a>.</strong> Todos los derechos reservados.
      </footer>

      
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
    
  </body>
</html>
