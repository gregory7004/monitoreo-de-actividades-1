@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar Actividad NO.: {{$actividades->Id_Actividad_}}</h3>
        @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
        @endif
        </div>
</div>

{!!Form::model($actividades,['method'=>'PATCH','route'=>['actividades.update', $actividades->Id_Actividad_]])!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="Descripcion_Actividad">Descripción de la Actividad:</label>
            <textarea name="Descripcion_Actividad" class='form-control' cols="30" rows="4">{{$actividades->Descripcion_Actividad}}</textarea>
        </div>
        <div class="form-group">
            <label for="Fecha_Inicio">Fecha de inicio: </label>
            <input type="date" name="Fecha_Inicio" class='form-control'>
        </div>
        <div class="form-group">
            <label for="Fecha_Fin">Fecha de Finalización</label>
            <input type="date" name="Fecha_Fin" class='form-control'>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
    <div class="form-group">
            <label>Seleccionar Meta Correspondiente:</label>
            <select name="MetaFK" class='form-control'>
                @foreach($meta as $m)
                <option value="{{$m->Id_Meta}}">{{$m->Descripcion}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Seleccionar Persona encargada de la Actividad:</label>
            <select name="Usuario_Responsable_Actividad" class='form-control'>
                @foreach($usuario as $m)
                <option value="{{$m->idUsuarios}}">{{$m->Nombres}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-group">
    <button class='btn btn-success' type="submit">Guardar</button>
    <button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}

@endsection