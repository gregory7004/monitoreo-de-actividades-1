@extends ('/layouts.admin')
@section('contenido')

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
        <h3>Lista de Actividades</h3>
        <!-- <a href="objetivo/create"><button class='btn btn-success'>Nuevo</button></a> -->
        @include('actividades.search')
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="tabe-responsive">
            <table class='table table-striped table-bordered table-condensed table-hover'>
                <head>
                    <th>ID</th>
                    <th>Descripción de la actividad</th>
                    <th>Usuario Responsable</th>
                    <th>Descripción de la Meta</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Finalización</th>
                    <th>Estado</th>
                    <!-- <th>Progreso</th> -->
                    <th>Opciones</th>
                </head>
                @foreach($actividades as $cat)
                <tr>
                    <td>{{$cat->Id_Actividad_}}</td>
                    <td>{{$cat->Descripcion_Actividad}}</td>
                    <td>{{$cat->usuario->Nombres}}</td>
                    <td>{{$cat->meta->Descripcion}}</td>
                    <td>{{$cat->Fecha_Inicio}}</td>
                    <td>{{$cat->Fecha_Fin}}</td>
                    <td>{{$cat->Estado}}</td>
                    <!-- <td>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div>
                    </td> -->
                    <td>
                    <a href="{{URL::action('ActividadesController@edit', $cat->Id_Actividad_)}}"><button class='btn btn-info'>Editar</button></a>
                        <a href="" data-target="#modal-delet-{{$cat->Id_Actividad_}}" data-toggle="modal"><button class='btn btn-danger'>Eliminar</button></a>
                    </td>
                </tr>
                @include('actividades.modal')
                @endforeach
            </table>
        </div>
       
    </div>
</div>
@endsection