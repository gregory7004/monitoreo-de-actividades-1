{!! Form::open(array('url'=>'actividades','method'=>'get','autocomplete'=>'off','role'=>'search'))!!}
    <div class="form-group">
        <div class="input-group">
            <input type="text" class="form-control" name='searchText' placeholder='Buscar actividad...' value='{{$searchText}}'>
            <span class='input-group-btn'>
                <button type='submit' class='btn btn-primary'>Buscar</button>
            </span>
        </div>
    </div>
{{Form::close()}}