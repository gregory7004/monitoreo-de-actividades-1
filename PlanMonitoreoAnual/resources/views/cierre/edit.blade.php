@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Cerrar Actividad NO.: {{$actividades->Id_Actividad_}}</h3>
        @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
        @endif
        </div>
</div>

{!!Form::model($actividades,['method'=>'PATCH','route'=>['cierreactividades.update', $actividades->Id_Actividad_]])!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="Descripcion_Actividad">Descripción de la Actividad:</label>
            <textarea name="Descripcion_Actividad" class='form-control' cols="30" rows="4" readonly>{{$actividades->Descripcion_Actividad}}</textarea>
        </div>
            <div class="form-group">
                <label>Meta Correspondiente:</label>
                <textarea name="" class='form-control' cols="30" rows="1" readonly>{{$actividades->meta->Descripcion}}</textarea>
            </div>
        <div class="form-group">
            <label>Persona encargada de la Actividad:</label>
            <textarea name="" class='form-control' cols="30" rows="1" readonly>{{$actividades->usuario->Nombres}}</textarea>
        </div>
        <div class="form-group">
            <label for="adjunto">Adjuntar archivo</label>
            <input type="file" name="anexos" class=''>
        </div>
        <div class="form-group">
            <label for="adjunto">Adjuntar imágen</label>
            <input type="file" name="imagen" class=''>
        </div>
    </div>
    <!-- <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12"> -->

    <!-- </div> -->
</div>
<div class="form-group">
    <button class='btn btn-warning' type="submit">Cerrar</button>
    <button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}

@endsection