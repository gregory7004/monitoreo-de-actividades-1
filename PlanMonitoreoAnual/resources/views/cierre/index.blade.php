@extends ('/layouts.admin')
@section('contenido')

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
        <h3>Lista de Actividades Pendientes</h3>
        <!-- <a href="objetivo/create"><button class='btn btn-success'>Nuevo</button></a> -->
        @include('cierre.search')
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="tabe-responsive">
            <table class='table table-striped table-bordered table-condensed table-hover'>
                <head>
                    <th>ID</th>
                    <th>Descripción de la actividad</th>
                    <th>Usuario Responsable</th>
                    <th>Descripción de la Meta</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Finalización</th>
                    <th>Estado</th>
                    <th>Opciones</th>
                </head>
                @foreach($actividades as $cat)
                <tr>
                    <td>{{$cat->Id_Actividad_}}</td>
                    <td>{{$cat->Descripcion_Actividad}}</td>
                    <td>{{$cat->usuario->Nombres}}</td>
                    <td>{{$cat->meta->Descripcion}}</td>
                    <th>{{$cat->Fecha_Inicio}}</th>
                    <th>{{$cat->Fecha_Fin}}</th>
                    <th>{{$cat->Estado}}</th>
                    <!-- <td>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div>
                    </td> -->
                    <td>
                    <a href="{{URL::action('CierreController@edit', $cat->Id_Actividad_)}}"><button class='btn btn-warning'>Cerrar</button></a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
       
    </div>
</div>
@endsection