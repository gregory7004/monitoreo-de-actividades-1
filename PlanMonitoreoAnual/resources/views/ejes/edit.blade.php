@extends ('/layouts.admin')
@section('contenido')
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<h3>Editar Eje ID NO.: {{$ejes->idEjes}}</h3>
@if(count($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif


{!!Form::model($ejes,['method'=>'PATCH','route'=>['ejes.update', $ejes->idEjes]])!!}
{{Form::token()}}
    <div class="form-group">
        <label for="Descripcion_eje">Descripción Eje:</label>
        <textarea name="Descripcion_eje" class='form-control' rows="4" >{{$ejes->Descripcion_eje}}</textarea>
    </div>
    <div class="form-group">
        <select name="Unidad_administrativa" class='form-control'>
            @foreach($unidad as $u)
                <option value="{{$u->idUnidad_administrativa}}">{{$u->Descripcion_unidad}}</option>
            @endforeach
        </select>
        <!-- <label for="Unidad_administrativa">Unidad Administrativa:</label>
        <input type="text" name='Unidad_administrativa' class='form-control' value="{{$ejes->Unidad_administrativa}}"> -->
    </div>
<div class="form-group">
<button class='btn btn-success' type="submit">Guardar</button>
<button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}
</div>
</div>
@endsection