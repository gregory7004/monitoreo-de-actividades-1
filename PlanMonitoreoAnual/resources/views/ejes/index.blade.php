@extends ('/layouts.admin')
@section('contenido')

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
        <h3>Listado de Ejes </h3>
        <!-- <a href="ejes/create"><button class='btn btn-success'>Nuevo</button></a> -->
        @include('ejes.search')
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="tabe-responsive">
            <table class='table table-striped table-bordered table-condensed table-hover'>
                <head>
                    <th>ID</th>
                    <th>Descripción del Eje</th>
                    <th>Descripción de la Unidad</th>
                    <th>Progreso</th>
                    <th>Estado</th>
                    <th>Opciones</th>
                </head>
                @foreach ($ejes as $Ejemplo)
                <tr>
                    <td>{{ $Ejemplo->idEjes }}</td>
                    <td>{{ $Ejemplo->Descripcion_eje}}</td>
                    <td>{{ $Ejemplo->unidad->Descripcion_unidad}}</td>
                    <td>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div>
                    </td>
                    <td>Activado</td>
                    <td>
                        <a href="{{URL::action('EjesController@edit', $Ejemplo->idEjes)}}"><button class='btn btn-info'>Editar</button></a>
                        <a href="" data-target="#modal-delet-{{$Ejemplo->idEjes}}" data-toggle="modal"><button class='btn btn-danger'>Desactivar</button></a>
                    </td>
                </tr>
                @include('ejes.modal')
                @endforeach
            </table>
        </div>

    </div>
</div>
@endsection