<div class='modal fade modal-slide-in-right' aria-hidden="true" rol="dialog" tabindex="-1" id="modal-delet-{{$Ejemplo->idEjes}}">
    {{Form::Open(array('action'=>array('EjesController@destroy', $Ejemplo->idEjes), 'method'=>'delete'))}}

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" arial-label="Close">
                        <span area-hidden="true">X</span>
                    </button>
                    <h4 class="modal-title">Eliminar Eje</h4>
                </div>
                <div class="modal-body">
                    <p>¿Seguro desea eliminar el Eje: {{$Ejemplo->Descripcion_eje}}?</p>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-default' type="button" data-dismiss="modal">Cerrar</button>
                    <button class='btn btn-primary' type="submit">Confirmar</button>
                </div>
            </div>
        </div>

    {{Form::close()}}
</div>