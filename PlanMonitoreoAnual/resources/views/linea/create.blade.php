@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Nuevo Línea de Actuación</h3>
        @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
    </div>
</div>
{!!Form::Open(array('url'=>'lineaactuacion','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="Descripcion">Descripción de la Línea:</label>
            <textarea name="desc_linea_actuacion" class='form-control' cols="30" rows="4" placeholder='Descripcion del Objetivo...' ></textarea>
        </div>
        <div class="form-group">
            <label>Seleccionar Objetivo Estratégico correspondiente:</label>
            <select name="id_obj_estr" class='form-control'>
                @foreach( $objetivos as $o)
                <option value="{{$o->idObjetivos}}">{{$o->Descripcion_objetivo}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
    <div class="form-group">
        <button class='btn btn-success' type="submit">Guardar</button>
        <button class='btn btn-danger' type="reset">Cancelar</button>
    </div>
{!!Form::close()!!}
@endsection