@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar Línea de Actuación NO.: {{$linea->idlinea_actuacion}}</h3>
        @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
        @endif


{!!Form::model($linea,['method'=>'PATCH','route'=>['lineaactuacion.update', $linea->idlinea_actuacion]])!!}
{{Form::token()}}
<div class="form-group">
    <label for="Descripcion_eje">Descripción de la Línea:</label>
    <textarea name="desc_linea_actuacion" class='form-control' rows="4" >{{$linea->desc_linea_actuacion}}</textarea>
</div>
<div class="form-group">
    <label for="Id_eje">Seleccionar Objetivo Estratégico:</label>
    <select name="id_obj_estr" class='form-control'>
        @foreach($objetivos as $o)
            <option value="{{$o->idObjetivos}}">{{$o->Descripcion_objetivo}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <button class='btn btn-success' type="submit">Guardar</button>
    <button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}
</div>
</div>
@endsection