@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Registrar Nueva Meta</h3>
        @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
    </div>
</div>
{!!Form::Open(array('url'=>'meta','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="Descripcion">Descripción de la Meta:</label>
            <textarea name="Descripcion" class='form-control' cols="30" rows="4" placeholder='Descripcion de la Meta...' ></textarea>
        </div>
        <div class="form-group">
            <label>Seleccionar Objetivo Táctico:</label>
            <select name="obje_tac" class='form-control'>
                @foreach($Objetivos as $e)
                <option value="{{$e->idobjetivo_tactico}}">{{$e->desc_objetivo}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
    <div class="form-group">
        <button class='btn btn-success' type="submit">Guardar</button>
        <button class='btn btn-danger' type="reset">Cancelar</button>
    </div>
{!!Form::close()!!}
@endsection