@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar Meta ID NO.: {{$Meta->Id_Meta}}</h3>
        @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
        @endif


{!!Form::model($Meta,['method'=>'PATCH','route'=>['meta.update', $Meta->Id_Meta]])!!}
{{Form::token()}}
<div class="form-group">
    <label for="Descripcion_eje">Descripción de la Meta:</label>
    <textarea name="Descripcion" class='form-control' rows="4" >{{$Meta->Descripcion}}</textarea>
</div>
<div class="form-group">
    <label>Seleccionar Objetivo Táctico:</label>
            <select name="obje_tac" class='form-control'>
                @foreach($Objetivo as $e)
                <option value="{{$e->idobjetivo_tactico}}">{{$e->desc_objetivo}}</option>
                @endforeach
            </select>
</div>
<div class="form-group">
    <button class='btn btn-success' type="submit">Guardar</button>
    <button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}
</div>
</div>
@endsection