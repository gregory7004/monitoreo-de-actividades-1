@extends ('/layouts.admin')
@section('contenido')

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
        <h3>Listado de Metas </h3>
        <!-- <a href="meta/create"><button class='btn btn-success'>Nuevo</button></a> -->
        @include('meta.search')
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="tabe-responsive">
            <table class='table table-striped table-bordered table-condensed table-hover'>
                <head>
                    <th>ID</th>
                    <th>Descripción de la Meta</th>
                    <th>Descripción del Objetivo Táctico</th>
                    <th>Progreso</th>
                    <th>Estado</th>
                    <th>Opciones</th>
                </head>
                @foreach($Meta as $cat)
                <tr>
                    <td>{{$cat->Id_Meta}}</td>
                    <td>{{$cat->Descripcion}}</td>
                    <td>{{$cat->objetivosp->desc_objetivo}}</td>  
                    <td>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div>
                    </td>
                    <td>Activado</td>                
                    <td>
                        <a href="{{URL::action('MetasController@edit', $cat->Id_Meta)}}"><button class='btn btn-info'>Editar</button></a>
                        <a href="" data-target="#modal-delet-{{$cat->Id_Meta}}" data-toggle="modal"><button class='btn btn-danger'>Eliminar</button></a>
                    </td>
                </tr>
               
                @endforeach
            </table>
        </div>
       
    </div>
</div>
@endsection