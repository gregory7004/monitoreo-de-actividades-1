@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Nuevo Objetivo Estratégico</h3>
        @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
    </div>
</div>
{!!Form::Open(array('url'=>'objetivo','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="Descripcion">Descripción del Objetivo:</label>
            <textarea name="Descripcion_objetivo" class='form-control' cols="30" rows="4" placeholder='Descripcion del Objetivo...' ></textarea>
        </div>
        <div class="form-group">
            <label>Seleccionar Eje:</label>
            <select name="Id_eje" class='form-control'>
                @foreach($ejes as $e)
                <option value="{{$e->idEjes}}">{{$e->Descripcion_eje}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="Presupuesto">Asignar Presupuesto:</label>
            <input type="text" name="Presupuesto" class='form-control' placeholder='Presupuesto...'>
        </div>
    </div>
</div>
    <div class="form-group">
        <button class='btn btn-success' type="submit">Guardar</button>
        <button class='btn btn-danger' type="reset">Cancelar</button>
    </div>
{!!Form::close()!!}
@endsection