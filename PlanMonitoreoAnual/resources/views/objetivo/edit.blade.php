@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar Objetivo Estratégico NO.: {{$Objetivo->idObjetivos}}</h3>
        @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            @endforeach
        </ul>
</div>
        @endif


{!!Form::model($Objetivo,['method'=>'PATCH','route'=>['objetivo.update', $Objetivo->idObjetivos]])!!}
{{Form::token()}}
<div class="form-group">
    <label for="Descripcion_eje">Descripción del Objetivo:</label>
    <textarea name="Descripcion_objetivo" class='form-control' rows="4" >{{$Objetivo->Descripcion_objetivo}}</textarea>
</div>
<div class="form-group">
    <label for="Id_eje">Seleccionar Eje:</label>
    <select name="Id_eje" class='form-control'>
        @foreach($ejes as $e)
            <option value="{{$e->idEjes}}">{{$e->Descripcion_eje}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="Presupuesto">Modificar Presupuesto:</label>
    <input type="text" name='Presupuesto' class='form-control' value="{{$Objetivo->Presupuesto}}">
</div>
<div class="form-group">
    <button class='btn btn-success' type="submit">Guardar</button>
    <button class='btn btn-danger' type="reset">Cancelar</button>
</div>
{!!Form::close()!!}
</div>
</div>
@endsection