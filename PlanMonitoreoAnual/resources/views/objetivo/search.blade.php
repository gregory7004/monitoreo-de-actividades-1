{!! Form::open(array('url'=>'objetivo','method'=>'get','autocomplete'=>'off','role'=>'search'))!!}
<div class="form-group">
<div class="input-group">
<input type="text" class="form-control" name='searchText' placeholder='Buscar eje...' value='{{$searchText}}'>
<span class='input-group-btn'>
<button type='submit' class='btn btn-primary'>Buscar</button>
</span>
</div>
</div>
{{Form::close()}}