@extends ('/layouts.admin')
@section('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Nuevo Objetivo Táctico</h3>
        @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
    </div>
</div>
{!!Form::Open(array('url'=>'objetivotactico','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
            <label for="desc_objetivo">Descripción del Objetivo Táctico:</label>
            <textarea name="desc_objetivo" class='form-control' cols="30" rows="4" placeholder='Descripcion del Objetivo Táctico...' ></textarea>
        </div>
        <div class="form-group">
            <label>Seleccionar Línea de Actuación Correspondiente:</label>
            <select name="id_linea_actuacion" class='form-control'>
                @foreach($linea as $l)
                <option value="{{$l->idlinea_actuacion}}">{{$l->desc_linea_actuacion}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
    <div class="form-group">
        <button class='btn btn-success' type="submit">Guardar</button>
        <button class='btn btn-danger' type="reset">Cancelar</button>
    </div>
{!!Form::close()!!}
@endsection