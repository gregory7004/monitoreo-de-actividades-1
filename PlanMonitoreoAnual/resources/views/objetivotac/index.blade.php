@extends ('/layouts.admin')
@section('contenido')

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
        <h3>Listado de Objetivos Tácticos</h3>
        <!-- <a href="objetivo/create"><button class='btn btn-success'>Nuevo</button></a> -->
        @include('objetivotac.search')
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="tabe-responsive">
            <table class='table table-striped table-bordered table-condensed table-hover'>
                <head>
                    <th>ID</th>
                    <th>Descripción del Objetivo Táctico</th>
                    <th>Descripción Línea de Actuación</th>
                    <th>Progreso</th>
                    <th>Estado</th>
                    <th>Opciones</th>
                </head>
                @foreach($Objetivo as $cat)
                <tr>
                    <td>{{$cat->idobjetivo_tactico}}</td>
                    <td>{{$cat->desc_objetivo}}</td>
                    <td>{{$cat->linea->desc_linea_actuacion}}</td>
                    <td>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div>
                    </td>
                    <td>Activado</td>
                    <td>
                        <a href="{{URL::action('ObjetivoTacticoController@edit', $cat->idobjetivo_tactico)}}"><button class='btn btn-info'>Editar</button></a>
                        <a href="" data-target="#modal-delet-{{$cat->idobjetivo_tactico}}" data-toggle="modal"><button class='btn btn-danger'>Descativar</button></a>
                    </td>
                </tr>
                @include('objetivotac.modal')
                @endforeach
            </table>
        </div>
       
    </div>
</div>
@endsection