<div class='modal fade modal-slide-in-right' aria-hidden="true" rol="dialog" tabindex="-1" id="modal-delet-{{$cat->idobjetivo_tactico}}">
    {{Form::Open(array('action'=>array('ObjetivoTacticoController@destroy', $cat->idobjetivo_tactico), 'method'=>'delete'))}}

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" arial-label="Close">
                        <span area-hidden="true">X</span>
                    </button>
                    <h4 class="modal-title">Eliminar Objetivo</h4>
                </div>
                <div class="modal-body">
                    <p>¿Seguro desea eliminar el objetivo: {{$cat->desc_objetivo}}?</p>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-default' type="button" data-dismiss="modal">Cerrar</button>
                    <button class='btn btn-primary' type="submit">Confirmar</button>
                </div>
            </div>
        </div>

    {{Form::close()}}
</div>