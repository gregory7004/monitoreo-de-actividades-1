<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

/*
Carpeta dentro de vista (Views)
*/

Route::resource('ejes','EjesController');

Route::resource('objetivo','ObjetivosController');

Route::resource('meta','MetasController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('lineaactuacion','LineaActuacionController');

Route::resource('objetivotactico','ObjetivoTacticoController');

Route::resource('actividades','ActividadesController');

Route::resource('cierreactividades','CierreController');